<?php

$EM_CONF['template_grillcamp'] = [
    'title' => 'Site package',
    'description' => 'Template and site package for grillcamp-hamburg.de',
    'category' => 'fe',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'Jan Helke',
    'author_email' => 'info@quintanion.com',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.0.0-10.9.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
