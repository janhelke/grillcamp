<?php

$dotEnvFile = __DIR__ . '/../../.env';
if (file_exists($dotEnvFile)) {
    (new \Symfony\Component\Dotenv\Dotenv(true))->load($dotEnvFile);
}

$GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = getenv('BE_DEBUG');
$GLOBALS['TYPO3_CONF_VARS']['BE']['installToolPassword'] = getenv('BE_INSTALL_TOOL_PASSWORD');
$GLOBALS['TYPO3_CONF_VARS']['BE']['sessionTimeout'] = getenv('BE_SESSION_TIMEOUT');

$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'] = getenv('DB_NAME');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'] = getenv('DB_HOST');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'] = getenv('DB_PASSWORD');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'] = getenv('DB_USER');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['port'] = getenv('DB_PORT');

$GLOBALS['TYPO3_CONF_VARS']['FE']['compressionLevel'] = getenv('FE_COMPRESSION_LEVEL');
$GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = getenv('FE_DEBUG');

$GLOBALS['TYPO3_CONF_VARS']['GFX']['processor'] = getenv('GFX_PROCESSOR');
$GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_colorspace'] = getenv('GFX_PROCESSOR_COLORSPACE');
$GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_effects'] = getenv('GFX_PROCESSOR_EFFECTS');

$GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'] = getenv('MAIL_DEFAULT_FROM_ADDRESS');
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName'] = getenv('MAIL_DEFAULT_FROM_NAME');
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] = getenv('MAIL_TRANSPORT');
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_encrypt'] = getenv('MAIL_TRANSPORT_SMTP_ENCRYPT');
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_password'] = getenv('MAIL_TRANSPORT_SMTP_PASSWORD');
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_server'] = getenv('MAIL_TRANSPORT_SMTP_SERVER');
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username'] = getenv('MAIL_TRANSPORT_SMTP_USERNAME');
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_mbox_file'] = getenv('MAIL_TRANSPORT_MBOX_FILE');

$GLOBALS['TYPO3_CONF_VARS']['SYS']['clearCacheSystem'] = getenv('SYS_CLEAR_CACHE_SYSTEM');
$GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = getenv('SYS_DEV_IP_MASK');
$GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = getenv('SYS_DISPLAY_ERRORS');
$GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = getenv('SYS_ENABLE_DEPRECATION_LOG');
$GLOBALS['TYPO3_CONF_VARS']['SYS']['exceptionalErrors'] = getenv('SYS_EXCEPTIONAL_ERRORS');
$GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] = getenv('SYS_SITENAME');
$GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = getenv('SYS_SQL_DEBUG');
$GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] = getenv('SYS_LOG_LEVEL');
$GLOBALS['TYPO3_CONF_VARS']['SYS']['systemMaintainers'] = explode(',',getenv('SYS_SYSTEMMAINTAINERS'));
$GLOBALS['TYPO3_CONF_VARS']['SYS']['trustedHostsPattern'] = getenv('SYS_TRUSTED_HOSTS_PATTERN');
